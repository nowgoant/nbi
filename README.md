# nbi

# 安装

```
  npm i nbi -g
```

# 使用

![](https://github.com/nowgoant/nbi/blob/master/assest/img/qrcode.gif)

从当前目下 package.json 中的 dist 节点的 sourcePath 复制到 targetPath 下

```
  // package.json
  "dist": {
    "sourcePath": "",//源目录,相对目录
    "targetRoot": "",//[string|array]目标根目录,默认取用户目录
    "targetPath": "/Desktop" //目标目录
  },

  // 复制命令
  nbi cp [arg]

  // 打包zip命令
  // 打包package.json中的dist节点下的sourcePath路劲下所有文件
  nbi zip [arg]

  // 生成二维码
  // 升级成图片二维码，需要iTerm2 >= v3.1.6
  nbi qrcode [arg]
  // 例如：
  nbi qrcode https://m.jr.jd.com -c -s -o
  // -c 加时间挫
  https://m.jr.jd.com?t=1526877025108
  // -s 输出小二维码
  // -o 用google chrome打开URL
```
