#!/usr/bin/env node

'use strict';

const logger = require('../util/logger');
// const util = require('../util');
const utilPath = require('../util/path');
// const qrcode = require('qrcode-terminal');
const commander = require('commander');
const path = require('path');
const fs = require('fs-extra');

const CONFIGPATH = path.join(utilPath.ROOT_PATH, 'config');
const program = commander
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<options> [options]')
  .option('-b, --bowerrc', '.bowerrc config')
  .option('-e, --editorconfig', '.editorconfig config')
  .option('-l, --eslintrc', '.eslintrc config')
  .option('-a, --gitattributes', '.gitattributes config')
  .option('-i, --gitignore', '.gitignore config')
  .option('-p, --prettierrc', '.prettierrc config')
  .parse(process.argv);

// let args1 = program.args[0] || '';

const copyFile = (fileName, targetName) => {
  if (fileName) {
    targetName = targetName || fileName;
    fs
      .copy(path.join(CONFIGPATH, fileName), path.join(utilPath.CWD_PATH, targetName))
      .then(() => logger.success(`success create ${targetName}`))
      .catch(err => logger.error(err));
  }
};

// console.log('__dirname', __dirname);
// console.log('process.cwd()', process.cwd());

// logger.log(program.abc);
// logger.log(program.gitattributes);

if (program.bowerrc) {
  copyFile('.bowerrc');
}

if (program.editorconfig) {
  copyFile('.editorconfig');
}

if (program.eslintrc) {
  copyFile('.eslintrc');
}

if (program.gitattributes) {
  copyFile('.gitattributes');
}

if (program.gitignore) {
  copyFile('.gitignore_temp', '.gitignore');
}

if (program.prettierrc) {
  copyFile('.prettierrc');
}
