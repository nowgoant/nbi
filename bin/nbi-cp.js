#!/usr/bin/env node

'use strict'

const shelljs = require('shelljs')
const logger = require('../util/logger')
const path = require('path');
const utilPath = require('../util/path')
const program = require('commander').parse(process.argv)
const chalk = require('chalk')
const args0 = program.args[0] || ''
const args1 = program.args[1] || ''
const filePath = path.join(utilPath.CWD_PATH, 'package.json')
const fs = require('fs');

logger.log(chalk.white('xxx ' + args0 + ' ' + args1))

function cp(sourcePath, targetPath) {
  if (sourcePath && targetPath) {
    logger.log(chalk.white('$ cp -rf ' + sourcePath + ' to ' + targetPath))

    shelljs.cp('-rf', sourcePath, targetPath)

    logger.log(chalk.green('$ cp success, ' + sourcePath + ' to ' + targetPath))
  } else {
    logger.fatal('unfound sourcePath or targetPath in the dist' + filePath)
  }
}

if (args0 && args1) {
  cp(args0, path.join(utilPath.HOME_DIR, args1))
} else {
  if (!fs.existsSync(filePath)) {
    logger.fatal('unfound file :' + filePath)
  }

  const pkg = require(filePath)
  const dist = pkg.dist

  if (!dist) {
    logger.fatal('unfound dist in ' + filePath)
  }

  const sourcePath = pkg.dist.sourcePath
  const targetRoot = pkg.dist.targetRoot || utilPath.HOME_DIR
  const targetPath = pkg.dist.targetPath

  if (Array.isArray(targetRoot)) {
    if (targetRoot.length) {
      targetRoot.forEach(function(val) {
        const _root = val || utilPath.HOME_DIR
        const _targetPath = path.join(_root, targetPath)
        logger.log(chalk.white(_targetPath))
        if (fs.existsSync(_targetPath)) {
          cp(sourcePath, _targetPath)
        }
      });
    }
  } else {
    cp(sourcePath, path.join(targetRoot, targetPath))
  }

}

