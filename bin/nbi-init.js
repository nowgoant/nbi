#!/usr/bin/env node

'use strict';

const fs = require('fs');
const logger = require('../util/logger');
const exec = require('../util/exec');
const git = require('../util/git');
const shelljs = require('shelljs');
const path = require('path');

// const qrcode = require('qrcode-terminal');
const commander = require('commander');

const program = commander
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<options> [options]')
  .parse(process.argv);

let args1 = program.args[0] || '';
let args2 = program.args[1] || '';

logger.log('模板git地址:', args1);
logger.log('初始化git地址:', args2 || '空');

let filename;
let mvDir = path.join(process.cwd());
if (args2) {
  filename = path.basename(args2, '.git');
  mvDir = path.join(process.cwd(), filename);

  if (fs.existsSync(mvDir)) {
    logger.fatal(`初始化终止，原因：已存在文件夹，${mvDir}`);
  }

  logger.log('初始化git地址-文件夹', filename);
  exec('git', ['clone', args2, path.join(process.cwd(), filename)], {
    stdio: 'inherit'
  });
}

if (args1) {
  const dic = git.clone(args1);
  shelljs.cp('-Rf', path.join(dic, '/.[^.]*'), mvDir);
  shelljs.cp('-Rf', path.join(dic, '/*'), mvDir);
  shelljs.rm('-Rf', dic);
  logger.success(`success init by ${args1}`);
}
