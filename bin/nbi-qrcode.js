#!/usr/bin/env node

'use strict';

const logger = require('../util/logger');
const util = require('../util');
// const qrcode = require('qrcode-terminal');
const commander = require('commander');
const opn = require('opn');
const QRCode = require('qrcode');
const base64Img = require('base64-img');
const termImg = require('term-img-x');
const fs = require('fs');

const program = commander
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<options> [options]')
  .option('-c, --clearCache', 'clear cache for the url')
  .option('-o, --open', 'open in chrome')
  .option('-s, --size [value]', 'size for width and height')
  .parse(process.argv);

let args1 = program.args[0] || '';

if (program.clearCache) {
  args1 = util.clearCacheUrl(args1);
}

logger.log('qrcode的入参:', args1);

if (args1) {
  // qrcode.generate(args1, { small: program.small }, function(qrcode) {
  //   logger.success('输出的二维码:');
  //   console.log(qrcode);
  // });

  const defaultSize = 400;
  let width = defaultSize;
  let height = defaultSize;
  try {
    if (program.size) {
      const splits = program.size.split('x');
      width = Number(splits[0]) || defaultSize;
      height = Number(splits[1]) || defaultSize;
    }
  } catch (error) {
    width = defaultSize;
    height = defaultSize;
  }

  // console.log('size', width, height);

  QRCode.toDataURL(
    args1,
    {
      errorCorrectionLevel: 'Q',
      width: width,
      height: height
    },
    function(err, base64) {
      // console.log(base64);
      base64Img.img(base64, '', 'nbi_qrcode', function(err, filePath) {
        // console.log(filePath);
        termImg(filePath, {
          noCheckITermApp: true
        });

        fs.unlink(filePath, function(error) {
          if (error) {
            throw error;
          }
        });
      });
    }
  );

  if (program.open) {
    opn(args1, { app: ['google chrome', '--incognito'], wait: false });
  }
}
