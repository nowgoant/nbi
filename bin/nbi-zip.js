#!/usr/bin/env node

'use strict';

// const shelljs = require('shelljs');
const logger = require('../util/logger');
const path = require('path');
const utilPath = require('../util/path');
// const exec = require('../util/exec');
// const program = require('commander').parse(process.argv);
const chalk = require('chalk');
// const args1 = program.args[0] || '-r';
const filePath = path.join(utilPath.CWD_PATH, 'package.json');
const fs = require('fs');
const archiver = require('archiver');

if (!fs.existsSync(filePath)) {
  logger.fatal(`unfound file :${filePath}`);
}

const pkg = require(filePath);
const dist = pkg.dist;

if (!dist) {
  logger.fatal(`unfound dist in ${filePath}`);
}

function zip(sourcePath) {
  sourcePath = path.join(utilPath.CWD_PATH, sourcePath);
  if (fs.existsSync(sourcePath)) {
    const name = path.basename(sourcePath);
    // shelljs.cd(sourcePath);

    // exec('zip', [args1, `${name}.zip`, '.']);
    const outputFile = path.join(sourcePath, `${name}.zip`);
    const output = fs.createWriteStream(outputFile);
    const archive = archiver('zip', {
      zlib: { level: 9 } // Sets the compression level.
    });
    // good practice to catch this error explicitly
    archive.on('error', function(err) {
      throw err;
    });
    // pipe archive data to the file
    archive.pipe(output);

    archive.directory(sourcePath, false);

    archive.finalize();

    logger.log(chalk.green(`$ zip success, ${outputFile}`));
  } else {
    logger.fatal(`unfound sourcePath in the dist${sourcePath}`);
  }
}

const sourcePath = pkg.dist.sourcePath;

zip(sourcePath);
