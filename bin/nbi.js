#!/usr/bin/env node

'use strict';

const path = require('path');
const program = require('commander');
const PATH = require('../util/path');
// const chalk = require('chalk');
// const logger = require('../util/logger');
const pkg = require('./../package.json');
const config = require('../util/config');

config.init();

// hack
// https://gist.github.com/branneman/8048520#6-the-hack
process.env.NODE_PATH = (process.env.NODE_PATH || '') + ['', path.join(PATH.CWD_PATH, 'node_modules'), path.join(PATH.ROOT_PATH, 'node_modules'), PATH.HOME_DIR].join(path.delimiter);

require('module').Module._initPaths();

program
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<command> [options]')
  .version(pkg.version)
  .command('cp <args>', '从package.json的dist节点复制')
  .command('zip <args>', '打包package.json中的dist节点下的sourcePath路劲下所有文件')
  .command('qrcode <args>', '在terminal中生成二维码,例如生成URL二维码')
  .command('config <args>', '生成配置文件')
  .command('init <args>', '用Git地址初始化工程,含两个参数，第一个参数是源Git地址,第二个参数是初始化的Git地址')
  .parse(process.argv);

if (!process.argv.slice(2).length) {
  program.outputHelp();
}
