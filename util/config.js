const fs = require('fs');

const PLUGIN_PATH = require('./path').PLUGIN_PATH;

module.exports.init = () => {
  if (!fs.existsSync(PLUGIN_PATH)) {
    // fs.mkdir(PLUGIN_PATH);
    fs.mkdir(PLUGIN_PATH, 0777, function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log('creat done!');
      }
    });
  }
};
