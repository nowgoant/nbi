const shelljs = require('shelljs');
const path = require('path');
const exec = require('./exec');
const logger = require('./logger');

const PLUGIN_PATH = require('./path').PLUGIN_PATH;

const git = (options, registry) => {
  if (registry) {
    options.push(registry);
  }

  const pwd = shelljs.pwd().stdout;

  shelljs.cd(PLUGIN_PATH);

  const filename = path.basename(registry, '.git');
  logger.log('filename ', filename);

  exec('git', options, {
    stdio: 'inherit'
  });

  shelljs.cd(filename);
  shelljs.rm('-rf', '.git');
  logger.log('remove .git');
  shelljs.cd(pwd);

  return path.join(PLUGIN_PATH, filename);
};

module.exports.clone = registry => git(['clone'], registry);
