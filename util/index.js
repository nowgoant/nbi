
function clearCacheUrl(url) {
  const timestamp = (new Date()).valueOf();
  if (url.indexOf('?') >= 0) {
    url = `${url}&t=${timestamp}`;
  } else {
    url = `${url}?t=${timestamp}`;
  }
  return url;
}


module.exports = {
  clearCacheUrl
};
